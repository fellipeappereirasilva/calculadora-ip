package view;

import control.Calculos;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import modell.Resultados;

/**
 *
 * @author Fellipe
 */
public class TelaCalculadora extends javax.swing.JFrame {

    public TelaCalculadora() {

        initComponents();
        setaFormatIP();
    }

    private void setaFormatIP() {
        try {

            this.formatIP = new MaskFormatter("###.###.###.###");
            jFormattedTxtIP.setFormatterFactory(new DefaultFormatterFactory(formatIP));

        } catch (ParseException ex) {
            Logger.getLogger(TelaCalculadora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jFormattedTxtIP = new javax.swing.JFormattedTextField();
        jTextFieldTXTHost = new javax.swing.JTextField();
        jTextFieldTxtSub1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabelTxt_D_Ms = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabelTxt_D_Mr = new javax.swing.JLabel();
        jLabelTxt_Mr = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabelTxt_MS = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabelTxt_S = new javax.swing.JLabel();
        jLabelTxt_H = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaTxt_EndSub = new javax.swing.JTextArea();
        jLabelTxt_QuantHr = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabelTxt_D_EndR = new javax.swing.JLabel();
        jLabelTxt_D_EndS = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabelTxt_EndRed = new javax.swing.JLabel();
        jLabelTxt_QuantHs = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Trabalho: Redes de Computadores");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
        jLabel1.setText("IP :");

        jLabel2.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
        jLabel2.setText("SUB : ");

        jLabel3.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
        jLabel3.setText("MÁQ : ");

        jFormattedTxtIP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTxtIPActionPerformed(evt);
            }
        });

        jTextFieldTxtSub1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTxtSub1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jFormattedTxtIP, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(68, 68, 68)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldTxtSub1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldTXTHost, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jFormattedTxtIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(jTextFieldTxtSub1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldTXTHost))
                .addContainerGap())
        );

        jLabel4.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
        jLabel4.setText("End. Sub.");

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
        jLabel5.setText("S : ");

        jLabelTxt_D_Ms.setBackground(new java.awt.Color(255, 255, 255));
        jLabelTxt_D_Ms.setText("\\");
            jLabelTxt_D_Ms.setOpaque(true);

            jLabel9.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
            jLabel9.setText("H : ");

            jLabelTxt_D_Mr.setBackground(new java.awt.Color(255, 255, 255));
            jLabelTxt_D_Mr.setText("\\");
                jLabelTxt_D_Mr.setOpaque(true);

                jLabelTxt_Mr.setBackground(new java.awt.Color(255, 255, 255));
                jLabelTxt_Mr.setOpaque(true);

                jLabel8.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
                jLabel8.setText("MR :");

                jLabelTxt_MS.setBackground(new java.awt.Color(255, 255, 255));
                jLabelTxt_MS.setOpaque(true);

                jLabel10.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
                jLabel10.setText("MS :");

                jLabelTxt_S.setBackground(new java.awt.Color(255, 255, 255));
                jLabelTxt_S.setOpaque(true);

                jLabelTxt_H.setBackground(new java.awt.Color(255, 255, 255));
                jLabelTxt_H.setOpaque(true);

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTxt_Mr, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(jLabelTxt_D_Mr, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelTxt_MS, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTxt_D_Ms, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelTxt_S, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelTxt_H, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                );
                jPanel1Layout.setVerticalGroup(
                    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabelTxt_S, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(7, 7, 7)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabelTxt_H, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTxt_D_Mr, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabelTxt_Mr, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel10)
                                    .addComponent(jLabelTxt_MS, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelTxt_D_Ms, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

                jTextAreaTxt_EndSub.setColumns(20);
                jTextAreaTxt_EndSub.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
                jTextAreaTxt_EndSub.setRows(5);
                jScrollPane1.setViewportView(jTextAreaTxt_EndSub);

                jLabelTxt_QuantHr.setBackground(new java.awt.Color(255, 255, 255));
                jLabelTxt_QuantHr.setOpaque(true);

                jLabel11.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
                jLabel11.setText("End. Red :");

                jButton1.setText("Calcular");
                jButton1.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton1ActionPerformed(evt);
                    }
                });

                jLabelTxt_D_EndR.setBackground(new java.awt.Color(255, 255, 255));
                jLabelTxt_D_EndR.setText("\\");
                    jLabelTxt_D_EndR.setOpaque(true);

                    jLabelTxt_D_EndS.setBackground(new java.awt.Color(255, 255, 255));
                    jLabelTxt_D_EndS.setText("\\");
                        jLabelTxt_D_EndS.setOpaque(true);

                        jLabel6.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
                        jLabel6.setText("Quan Host R : ");

                        jLabel7.setFont(new java.awt.Font("PT Serif", 1, 14)); // NOI18N
                        jLabel7.setText("Quan Host SB :");

                        jLabelTxt_EndRed.setBackground(new java.awt.Color(255, 255, 255));
                        jLabelTxt_EndRed.setOpaque(true);

                        jLabelTxt_QuantHs.setBackground(new java.awt.Color(255, 255, 255));
                        jLabelTxt_QuantHs.setOpaque(true);

                        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
                        jPanel2.setLayout(jPanel2Layout);
                        jPanel2Layout.setHorizontalGroup(
                            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(jButton1))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel11)
                                            .addComponent(jLabel7))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabelTxt_D_EndR, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(100, 100, 100)
                                                .addComponent(jLabel4))
                                            .addComponent(jLabelTxt_EndRed, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabelTxt_QuantHs, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabelTxt_QuantHr, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabelTxt_D_EndS, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
                        );
                        jPanel2Layout.setVerticalGroup(
                            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addComponent(jLabelTxt_D_EndS, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelTxt_D_EndR, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabelTxt_EndRed, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(35, 35, 35)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabelTxt_QuantHr, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel7)
                                            .addComponent(jLabelTxt_QuantHs, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton1))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE))
                                .addContainerGap())
                        );

                        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                        getContentPane().setLayout(layout);
                        layout.setHorizontalGroup(
                            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        );
                        layout.setVerticalGroup(
                            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                        );

                        pack();
                        setLocationRelativeTo(null);
                    }// </editor-fold>//GEN-END:initComponents

    private Resultados res = new Resultados();
    private MaskFormatter formatIP;
    private Calculos calc;
    private int sub, host;
    private String Ip;

    private int getValores() {

        try {
            sub = Integer.parseInt(jTextFieldTxtSub1.getText());
            host = Integer.parseInt(jTextFieldTXTHost.getText());
            Ip = jFormattedTxtIP.getText();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "PREENCHA TODOS OS CAMPOS");
            return -1;
        }

        calc = new Calculos(sub, host, res);
        return 0;
    }

    private void calculaSub_host() {

        String s, h;
        calc.quant_sub_host();

        s = String.valueOf(res.getSub());
        h = String.valueOf(res.getHost());
        jLabelTxt_S.setText(" " + s);
        jLabelTxt_H.setText(" " + h);

    }

    private void calculaMaskR_MaskS() {

        String maskRed, maskSub;

        calc.calcMaskDecimal();

        maskRed = calc.calculaMascara(res.getMaskRedDecimal());
        maskSub = calc.calculaMascara(res.getMaskSubDecimal());

        res.setMaskRed(maskRed);
        res.setMaskSub(maskSub);

        jLabelTxt_Mr.setText(maskRed);
        jLabelTxt_D_Mr.setText("\\ " + res.getMaskRedDecimal());
        jLabelTxt_MS.setText(maskSub);
        jLabelTxt_D_Ms.setText("\\ " + res.getMaskSubDecimal());

    }

    private void calculaEnderecos() {
        calc.calcEndereco(Ip, res.getMaskRed());

        jLabelTxt_EndRed.setText(res.getEndRed());
        jLabelTxt_D_EndR.setText("\\ " + res.getMaskRedDecimal());

        for (int i = 0; i < res.getEndSub().size(); i++) {
            String s = (String) res.getEndSub().get(i);
            jTextAreaTxt_EndSub.append((i + 1) + " - " + s + '\n');
        }
        jLabelTxt_D_EndS.setText("\\ " + res.getMaskSubDecimal());

    }

    private void calculaQuantHosts() {
        calc.quantHosts();
        jLabelTxt_QuantHr.setText(" " + res.getQuantHostRed());
        jLabelTxt_QuantHs.setText(" " + res.getQuantHostSub());
    }


    private void jFormattedTxtIPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTxtIPActionPerformed
           
    }//GEN-LAST:event_jFormattedTxtIPActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int r;
        jTextAreaTxt_EndSub.setText(null);
        r = getValores();
        if (r != 0) {
            return;
        }
        calculaSub_host();
        calculaMaskR_MaskS();
        calculaEnderecos();
        calculaQuantHosts();
        calc.setaObjeto();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextFieldTxtSub1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTxtSub1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTxtSub1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaCalculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaCalculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaCalculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaCalculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaCalculadora().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JFormattedTextField jFormattedTxtIP;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelTxt_D_EndR;
    private javax.swing.JLabel jLabelTxt_D_EndS;
    private javax.swing.JLabel jLabelTxt_D_Mr;
    private javax.swing.JLabel jLabelTxt_D_Ms;
    private javax.swing.JLabel jLabelTxt_EndRed;
    private javax.swing.JLabel jLabelTxt_H;
    private javax.swing.JLabel jLabelTxt_MS;
    private javax.swing.JLabel jLabelTxt_Mr;
    private javax.swing.JLabel jLabelTxt_QuantHr;
    private javax.swing.JLabel jLabelTxt_QuantHs;
    private javax.swing.JLabel jLabelTxt_S;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaTxt_EndSub;
    private javax.swing.JTextField jTextFieldTXTHost;
    private javax.swing.JTextField jTextFieldTxtSub1;
    // End of variables declaration//GEN-END:variables
}
