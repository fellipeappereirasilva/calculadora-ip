package control;

import java.util.ArrayList;
import modell.Resultados;

/**
 *
 * @author Fellipe
 */
public class Calculos {

    private int sub;
    private int host;
    private Resultados result;

//    public Calculos() {
//
//    }
    public Calculos(int sub, int host, Resultados result) {
        this.sub = sub;
        this.host = host;
        this.result = result;
    }

    public void quant_sub_host() {
        int i = 1, aux = 0;

        while (i < sub) {
            i = i * 2;
            aux++;
          
        }
        sub = aux;
        i = 1;
        aux = 0;
        while (i < host) {
            i = i * 2;
            aux++;
           
        }
        host = aux;

        result.setSub(sub);
        result.setHost(host);
    }

    
    public void quantHosts(){
        int i =1, r = (host + sub),j=0;
        
        while(j < host){
            i*=2;
            j++;
        }
        result.setQuantHostSub(i);
        j=0;
        i = 1;
        while(j < r){
            i*=2;
            j++;
        }
        result.setQuantHostRed(i);
    }
    
    
    public void calcMaskDecimal() {
        int maskRedDecimal = 32 - (host + sub);
        int maskSubDecimal = maskRedDecimal + sub;
        result.setMaskRedDecimal(maskRedDecimal);
        result.setMaskSubDecimal(maskSubDecimal);
    }

    public String calculaMascara(int maskDecimal) {
        int i = 0, diferenca = 0, casaCheia = 0;
        String resultado = null, aux = null;

        while (i < maskDecimal) {
            i += 8;
            if (i <= maskDecimal) {
                casaCheia++;
            } else {
                i -= 8;
                diferenca = maskDecimal - i;
                break;
            }

        }

        switch (diferenca) {
            case 1:
                aux = "128";
                break;
            case 2:
                aux = "192";
                break;
            case 3:
                aux = "224";
                break;
            case 4:
                aux = "240";
                break;
            case 5:
                aux = "248";
                break;
            case 6:
                aux = "252";
                break;
            case 7:
                aux = "254";
                break;
            default:
                aux = "0";
        }

        switch (casaCheia) {
            case 1:
                resultado = "255.";
                resultado = resultado.concat(aux + ".0.0");
                break;
            case 2:
                resultado = "255.255.";
                resultado = resultado.concat(aux + ".0");
                break;
            case 3:
                resultado = "255.255.255.";
                resultado += aux;
                break;
            case 4:
                resultado = "255.255.255.255";
                break;

        }

        return resultado;
    }

    public void converteString(String mascara, ArrayList valores) {

        int i;
        String aux = null, x;

        for (i = 0; i < mascara.length(); i++) {

            if (mascara.charAt(i) != '.') {
                x = String.valueOf(mascara.charAt(i));
                if (aux == null) {
                    aux = x;
                } else {
                    aux = (aux + x);
                }
            } else {
                int j = Integer.parseInt(aux);
                valores.add(j);
                aux = null;
            }
        }

        int j = Integer.parseInt(aux);
        valores.add(j);

    }

    public void converteParaBinarios(int valor, ArrayList binario) {

        ArrayList aux = new ArrayList();
        int i;

        while (valor >= 1) {
            if (valor % 2 == 0) {
                aux.add(0);
            } else {
                aux.add(1);
            }
            valor /= 2;
        }
        while (aux.size() < 8) {
            aux.add(0);
        }

        i = aux.size() - 1;

        while (i >= 0) {
            binario.add(aux.get(i));
            i--;
        }

    }

    public void calculaBinarios(ArrayList binIp, ArrayList binMr, ArrayList result) {

        int i = 0, aux1, aux2;

        while (i < 8) {
            aux1 = (int) binIp.get(i);
            aux2 = (int) binMr.get(i);
            if (aux1 + aux2 == 2) {
                result.add(1);
            } else {
                result.add(0);
            }

            i++;
        }

    }

    public void calcEndereco(String endIp, String strMaskRed) {
        //este método calcula endereço de rede
        ArrayList formatIpDecimal = new ArrayList();
        ArrayList enderecoRed = new ArrayList();
        ArrayList maskRed = new ArrayList();
        String strEndRed = "";
        int i = 0, auxIp, auxMask;

        converteString(endIp, formatIpDecimal);
        converteString(strMaskRed, maskRed);

        while (i < formatIpDecimal.size()) {
            auxIp = (int) formatIpDecimal.get(i);
            auxMask = (int) maskRed.get(i);

            if (auxMask == 255) {
                enderecoRed.add(auxIp);
                strEndRed += (auxIp + ".");
            } else if (auxMask == 0 || auxIp == 0) {
                enderecoRed.add(0);
                strEndRed += (".0");
            } else {
                ArrayList binIp = new ArrayList();
                ArrayList binMask = new ArrayList();
                ArrayList resultSoma = new ArrayList();
                converteParaBinarios(auxIp, binIp);
                converteParaBinarios(auxMask, binMask);
                calculaBinarios(binIp, binMask, resultSoma);
                int decimal = binarioParaDecimal(resultSoma);
                enderecoRed.add(decimal);
                strEndRed += decimal;

            }
            i++;

        }
        System.out.println("End Red " + strEndRed);
        result.setEndRed(strEndRed);
        int mRd = result.getMaskRedDecimal();
        calEnderecoSub(mRd, enderecoRed);
    }

    private void calEnderecoSub(int maskRedDecimal, ArrayList endRed) {

        int i = 0, casaCheia = 0, diferenca = 0, aux,
                aux02, variavel, j;
        ArrayList binario = new ArrayList();
        ArrayList binAux = new ArrayList();
        ArrayList endSub = new ArrayList();
        String strEndSub = "";
        while (i < maskRedDecimal) {
            i += 8;
            if (i <= maskRedDecimal) {
                casaCheia++;
            } else {
                i -= 8;
                diferenca = maskRedDecimal - i;
                break;
            }
        }

        aux = (int) endRed.get(casaCheia);
        converteParaBinarios(aux, binario);

        j = diferenca;
        //refetira e pega so o seu
        while (j < binario.size()) {
            int z = (int) binario.get(j);
            binAux.add(z);
            j++;
            //  i--;
        }

        variavel = sub;
        i = 0;

        while (variavel > 0) {

            if (variavel == 1) {
                if (i < binAux.size()) {
                    binAux.set(i, 1);
                    j = binarioParaDecimal(binAux);
                } else {

                    ArrayList binario02 = new ArrayList();
                    casaCheia++;
                    aux02 = (int) endRed.get(casaCheia);
                    converteParaBinarios(aux02, binario02);
                    variavel = sub - binAux.size();
                    i = 0;
                    while (variavel > 0) {

                        if (variavel == 1) {
                            binario02.set(i, 1);
                        }
                        i++;
                        variavel--;
                    }
                    j = binarioParaDecimal(binario02);
                }
                break;
            }
            i++;
            variavel--;
        }

        endSub = endRed;
        if (sub % 2 != 0) {
            variavel = (sub + 1);
        } else {
            variavel = sub;
        }

        variavel = (variavel * 2);
        ArrayList strEnderecosSub = new ArrayList();

        for (int y = 0; y < endSub.size(); y++) {
            if (y != 0) {
                strEndSub += "." + endSub.get(y);
            } else {
                strEndSub += endSub.get(y);
            }
        }
        strEnderecosSub.add(strEndSub);
        aux = (int) endSub.get(casaCheia);
        int auxCasa = casaCheia,z;
        boolean mudouCasa = false;
        i = 1;
        while (i < variavel) {
            
            aux += j;
            if(mudouCasa){
                z = (int) endSub.get(auxCasa);
                z++;
                endSub.set(auxCasa, z);
                mudouCasa = false;
            }
            if (aux >= 256) {
                auxCasa--;
                aux = 0;
                mudouCasa = true;
            }
            endSub.set(casaCheia, aux);
            strEndSub = "";
            for (int y = 0; y < endSub.size(); y++) {
                if (y != 0) {
                    strEndSub += "." + endSub.get(y);
                } else {
                    strEndSub += endSub.get(y);
                }
            }
            strEnderecosSub.add(strEndSub);
            i++;
        }

        result.setEndSub(strEnderecosSub);

    }

    public int binarioParaDecimal(ArrayList binario) {
        int decimal = 0, i, j = 1, aux;

        for (i = binario.size() - 1; i >= 0; i--) {
            aux = (int) binario.get(i);
            if (aux == 1) {
                decimal += j;
            }
            j *= 2;
        }

        return decimal;
    }

    public void setaObjeto() {
        for (int i = 0; i < result.getEndSub().size(); i++) {
            result.getEndSub().remove(i);
        }
    }

}
