package modell;

import java.util.ArrayList;

/**
 *
 * @author Fellipe
 */
public class Resultados {

    private int sub;
    private int host;
    private int quantHostRed;
    private int quantHostSub;

    public int getQuantHostRed() {
        return quantHostRed;
    }

    public void setQuantHostRed(int quantHostRed) {
        this.quantHostRed = quantHostRed;
    }

    public int getQuantHostSub() {
        return quantHostSub;
    }

    public void setQuantHostSub(int quantHostSub) {
        this.quantHostSub = quantHostSub;
    }
    private int maskRedDecimal;
    private int maskSubDecimal;
    private String maskRed;
    private String maskSub;
    private String endRed;
    private ArrayList endSub;
    
    public Resultados(){
        
    }

    public int getSub() {
        return sub;
    }

    public void setSub(int sub) {
        this.sub = sub;
    }

    public int getHost() {
        return host;
    }

    public void setHost(int host) {
        this.host = host;
    }

    public int getMaskRedDecimal() {
        return maskRedDecimal;
    }

    public void setMaskRedDecimal(int maskRedDecimal) {
        this.maskRedDecimal = maskRedDecimal;
    }

    public int getMaskSubDecimal() {
        return maskSubDecimal;
    }

    public void setMaskSubDecimal(int maskSubDecimal) {
        this.maskSubDecimal = maskSubDecimal;
    }

    public String getMaskRed() {
        return maskRed;
    }

    public void setMaskRed(String maskRed) {
        this.maskRed = maskRed;
    }

    public String getMaskSub() {
        return maskSub;
    }

    public void setMaskSub(String maskSub) {
        this.maskSub = maskSub;
    }

    public String getEndRed() {
        return endRed;
    }

    public void setEndRed(String endRed) {
        this.endRed = endRed;
    }

    public ArrayList getEndSub() {
        return endSub;
    }

    public void setEndSub(ArrayList endSub) {
        this.endSub = endSub;
    }

   

}
